<h2 align="center">
    LaRecipe
</h2>


<h6 align="center">
    Write gorgeous documentations for your products using Markdown inside your Laravel app.
</h6>
    

<br/><br/>

# LaRecipe 🍪

**LaRecipe** is simply a code-driven package provides an easy way to create beautiful documentation for your product or application inside your Laravel app.

![LaRecipe Screenshot](https://larecipe.binarytorch.com.my/images/screenshot.png#)

## Official tools and assets support

| Title | v1.x | v2.x |
| :- | :-: | :-: |
| [LaRecipe Dark Theme](https://larecipe.binarytorch.com.my/packages/binarytorch/larecipe-dark-theme) | ❌ | ✅ |
| [LaRecipe RTL Support](https://larecipe.binarytorch.com.my/packages/binarytorch/larecipe-rtl) | ❌ | ✅ |
| [LaRecipe Feedback](https://larecipe.binarytorch.com.my/packages/binarytorch/larecipe-feedback) | ❌ | ✅ |
| [LaRecipe Swagger](https://larecipe.binarytorch.com.my/packages/binarytorch/larecipe-swagger) | ❌ | ✅ |

## Getting Started

☝️ Install the package via composer.

    composer require binarytorch/larecipe

✌️ Run the install command.

    php artisan larecipe:install

Visit your app domain with `/docs` endpoint. That's it.

#### See [full documentation](https://larecipe.binarytorch.com.my/)


## Examples

* [Zino](https://zino.binarytorch.com.my/1.0/installation) - 🤖 Custom Arduino library made for humans.
* [Blogged](https://blogged.binarytorch.com.my/docs/1.0/overview) - Blogged is a package provides an easy way to create beautiful blog inside your Laravel projects.
* [Wave](https://wave.devdojo.com/docs) - Wave is the perfect starter kit for building your next great idea
* [Nova ADC](https://nova-adc.com/docs/1.0/overview) - Nova is a cloud-based (or on-premise) SaaS application delivery controller.
* [WooSignal](https://woosignal.com/docs/api/1.0/overview) - Fastest WooCommerce
App Templates.
* [AddChat](https://addchat-docs.classiebit.com/docs/1.0/introduction) - All-in-one multi-purpose Chat Widget For Laravel & Codeigniter websites.
* [RoadLeagues](https://roadleagues.com/docs/1.0/overview) - A cycling league management system with an API to allow data distrubution externally.
* Put your docs here 😍👌


